#!/usr/bin/env bash
#
# This pipe is an example to show how easy it is to create pipes for Bitbucket Pipelines.
#
# Required globals:
#   GCLOUD_COMPUTE_ZONE
#   GCLOUD_CONTAINER_CLUSTER
#   GCLOUD_PROJECT
#   GCLOUD_API_KEY_BASE64
#

source "$(dirname "$0")/common.sh"

info "Executing the pipe..."

# Required parameters
GCLOUD_COMPUTE_ZONE=${GCLOUD_COMPUTE_ZONE:?'GCLOUD_COMPUTE_ZONE variable missing.'}
GCLOUD_CONTAINER_CLUSTER=${GCLOUD_CONTAINER_CLUSTER:?'GCLOUD_CONTAINER_CLUSTER variable missing.'}
GCLOUD_PROJECT=${GCLOUD_PROJECT:?'GCLOUD_PROJECT variable missing.'}
GCLOUD_API_KEY_BASE64=${GCLOUD_API_KEY_BASE64:?'GCLOUD_API_KEY_BASE64 variable missing.'}

enable_debug

echo $GCLOUD_API_KEY_BASE64 | base64 -d > ~/.gcloud-gke-api-key.json
run gcloud auth activate-service-account --key-file ~/.gcloud-gke-api-key.json
run gcloud container clusters get-credentials "$GCLOUD_CONTAINER_CLUSTER" \
          --project "$GCLOUD_PROJECT" \
          --zone "$GCLOUD_COMPUTE_ZONE"

rm bitbucket-pipelines.yml

run kubectl apply --recursive -f .

if [[ "${status}" == "0" ]]; then
  success "All resources applied."
else
  fail "Error!"
fi
